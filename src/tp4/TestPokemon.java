package tp4;

public class TestPokemon {

	public static void main(String[] args) {
		//On cr�e un nouvel objet Pokemon qui portera le nom "Obama"
		Pokemon p1 = new Pokemon("Obama");
		p1.sePresenter();
		p1.manger();
		p1.sePresenter();
		p1.vivre();
		p1.vivre();
		p1.vivre();
		p1.sePresenter();

		//On v�rfie le cycle de vie d'un pok�mon
		Pokemon p2 = new Pokemon("Clinton");
		System.out.println("\nTest de cycle");
		p2.sePresenter();
		p2.cycleVie();

		//On v�rifie la perte d'�nergie d'un pokemon
		Pokemon p3 = new Pokemon("Washington");
		System.out.println("\nExercice 2");
		p3.sePresenter();
		p3.perdreEnergie(5);
		p3.sePresenter();
		System.out.println(p3.isAlive());



	}

}
