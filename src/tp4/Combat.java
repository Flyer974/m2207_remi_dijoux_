package tp4;

public class Combat {

	public static void main(String[] args) {
		//On cr�e 2 nouveaux objet Pokemon qui porterons les noms "Hillary Clinton" et "Donald Trump"
		Pokemon Hillary = new Pokemon("Hillary Clinton");
		Pokemon Trump = new Pokemon ("Donald Trump");

		//On utilise la methode "sePresenter()" pour pr�senter les deux pok�mons
		Hillary.sePresenter();
		Trump.sePresenter();

		System.out.println("--------------------------------------------------------------------------------------------------");

		//Trump attaque hillary
		Trump.attaquer(Hillary);

		int round=0;
		//On ajoute une boucle qui demandera que Trump et Hillary restent en vie pour continuer le combat
		while (Trump.isAlive() == true && Hillary.isAlive() == true ) {
			//On ajoute un compteur pour le nombre de rounds
			round++;
			Trump.attaquer(Hillary);
			Hillary.attaquer(Trump);
			System.out.println(("Round " +round+ " " + Trump.getNom() + " : (en) " + Trump.getEnergie() + " (atk) " + Trump.getPuissance() + " - "+ Hillary.getNom() + " : (en) " + Hillary.getEnergie() + " (atk) " + Hillary.getPuissance()));

		} 

		System.out.println("--------------------------------------------------------------------------------------------------");
		//On ajoute des conditions pour d�finir le vainqueur
		if (Trump.getEnergie() >0 ) {
			System.out.println(Trump.getNom()+" gagne en "+round+" rounds");
		} else if (Hillary.getEnergie()> 0){
			System.out.println(Hillary.getNom()+ " gagne en "+round+" rounds");
		}
		else {
			System.out.println("C'est un match nul, les deux pok�mons n'ont plus d'�nergie");
		}

	}
}