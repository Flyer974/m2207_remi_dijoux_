package tp4;

public class CombatAvecJoueur {

	public static void main(String[] args) {
		//On cr�e deux objet pokemon
		Pokemon Fillion = new Pokemon("Fillion");
		Pokemon Melenchon = new Pokemon("Melenchon");

		//On utilise les m�thodes "NomCombattant1() et NomCombattant2()" pour d�finir le nom des pok�mons
		Fillion.NomCombattant1();
		Melenchon.NomCombattant2();

		//On d�bute le combat entre Fillion et Melenchon
		int round=0;
		//On ajoute une boucle qui demandera que Fillion et Melenchon restent en vie pour continuer le combat
		while (Fillion.isAlive() == true && Melenchon.isAlive() == true ) {
			//On utilise un compteur pour les rounds
			round++;
			System.out.println("--------------------------------------------------------------------------------------------------");
			System.out.println("Round "+round);
			System.out.println(("Etat des combatants : "+Fillion.getNom() + " (en) " + Fillion.getEnergie() + " (atk) " + Fillion.getPuissance() + " / "+ Melenchon.getNom() + " (en) " + Melenchon.getEnergie() + " (atk) " + Melenchon.getPuissance()));

			//On utilise la m�thode "ChoixActionJoueur" pour que le Joueur puisse d�finir ses actions
			Fillion.ChoixActionJoueur();
			//L'action 1 va permettre a Fillion d'attaquer melenchon
			if (Fillion.action==1) {
				Fillion.CombatJoueur(Melenchon);
			//L'action 2 va permettre a Fillion de se nourrir et de r�cup�rer de l'energie
			}else if (Fillion.action==2){
				Fillion.manger();	
			}else {
				//On recommence les choix si la valeur 1 ou 2 n'a pas �t� mise
				Fillion.ChoixActionJoueur();
			}

			//On utilise la m�thode "ChoixActionJoueur" pour que le Joueur puisse d�finir ses actions
			Melenchon.ChoixActionJoueur();
			//L'action 1 va permettre a Melenchon d'attaquer melenchon
			if (Melenchon.action==1) {
				Melenchon.CombatJoueur(Fillion);
				//L'action 2 va permettre a Melenchon de se nourrir et de r�cup�rer de l'energie
			}else if (Melenchon.action==2){
				Melenchon.manger();	
			}else {
				//On recommence les choix si la valeur 1 ou 2 n'a pas �t� mise
				Melenchon.ChoixActionJoueur();
			}

		}

		//On ajoute des conditions pour d�finir le vainqueur
		if (Fillion.getEnergie() >0 ) {
			System.out.println("--------------------------------------------------------------------------------------------------");
			System.out.println(Fillion.getNom()+" gagne en "+round+" rounds");
		} else if (Melenchon.getEnergie()> 0){
			System.out.println("--------------------------------------------------------------------------------------------------");
			System.out.println(Melenchon.getNom()+ " gagne en "+round+" rounds");
		}
		else {
			System.out.println("--------------------------------------------------------------------------------------------------");
			System.out.println("C'est un match nul, les deux pok�mons n'ont plus d'�nergie");
		}


	}

}
