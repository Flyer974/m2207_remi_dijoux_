package tp4;
import java.util.Scanner; //Ceci nous permettra de capturer le clavier

public class Pokemon {
	//Attributs

	private int energie;
	private int maxEnergie;
	private String nom;
	private int puissance;
	private int round;
	private boolean furie;
	//On cr�e un nouvel objet scanner pour pourvoir capturer le clavier
	private Scanner scan = new Scanner(System.in);
	int action;
	//On cr�e un nouvel objet scanner pour pourvoir capturer le clavier
	private Scanner scanchoisir = new Scanner(System.in);
	int choisir;


	//Constructeur

	public Pokemon (String n) {
		this.nom=n; //On initialise "nom" dans la variable "n"
		maxEnergie = 50 +(int)(Math.random() * ((90 - 50) + 1)); //On d�finit ici l'�nergie maximun qui sera comprise entre 50 et 90 au hasard
		energie = 30 +(int)(Math.random() * ((maxEnergie - 30) + 1)); //On d�finit l'energie qui sera comprise entre 30 et l'energie maximun
		puissance = 3 +(int)(Math.random() * ((10 - 3) + 1)); //On d�finit la puissance qui sera comprise entre 3 et 10



	}
	//Accesseurs

	//Cet accesseur retournera le nom
	public String getNom() {
		return nom;
	}

	//Cet accesseur retournera la valeur de l'energie 
	public int getEnergie() {
		return energie;
	}

	//Cet accesseur permettra d'eviter d'utilser System.out.println()
	public void parler(String texte) {
		System.out.println(texte);
	}
	//Cet accesseur nous retournera la valeur de la puissance
	public int getPuissance( ) {
		return puissance;

	}
	//Cet accesseur nous retourne le nombre de round
	public int getRound () {
		return round;
	}

	//Cet accesseur nous permettra de donner le nom du premier pokemon
	public String NomCombattant1() {
		System.out.println("Veuillez saisir le nom du premier combattant");
		//On va pouvoir �crire le nom du pokemon grace � l'utilisation de scan
		return nom=scan.nextLine();
	}

	public String NomCombattant2() {
		//Cet accesseur nous permettra de donner le nom du deuxi�me pokemon
		System.out.println("Veuillez saisir le nom du deuxi�me combattant");
		//On va pouvoir �crire le nom du pokemon grace � l'utilisation de scan
		return nom=scan.nextLine();
	}
	
	public String NomCombattant3() {
		//Cet accesseur nous permettra de donner le nom du troisi�me pokemon
		System.out.println("Veuillez saisir le nom du troisi�me combattant");
		//On va pouvoir �crire le nom du pokemon grace � l'utilisation de scan
		return nom=scan.nextLine();
	}
	
	public String NomCombattant4() {
		//Cet accesseur nous permettra de donner le nom du quatri�me pokemon
		System.out.println("Veuillez saisir le nom du quatri�me combattant");
		//On va pouvoir �crire le nom du pokemon grace � l'utilisation de scan
		return nom=scan.nextLine();
	}

	
	
	
	//Methode
	
	//Cette m�thode va nous permettre de pr�senter le pok�mon
	public void sePresenter( ) {
		System.out.println(("Je suis "+getNom()+" j'ai "+energie+" points d'energie ("+maxEnergie+" max) et une puissance de "+getPuissance()));
	}

	//Cette m�thode va nous permettre de nourrir le pok�mon
	public void manger() {
		//Le pok�mon pourra se nourrir d'une valeur comprise en 10 et 30 points d'energie
		if (this.energie > 0) {
			energie+= 10 +(int)(Math.random() * (30 - 10) + 1); 
			
			//Le pok�mon ne pourra pas se nourrir si il n'a plus d'energie
		}else {parler(getNom()+" n'a plus d'energie pour se nourrir");
		}
		//Le pok�mon ne pourra pas se nourrir si son �nergie est � son maximun
		if (this.energie > maxEnergie) {
			energie=maxEnergie;
			parler(getNom()+ " a d�ja son �nergie au maximun et jette la nourriture qu'il reste");
		}
	}
	
	//Cette m�thode nous permettra de d�finir les points d'nergie qu'aura le pokemon. La valeur sera comprise entre 20 et 40
	public void vivre() {
		energie-= 20 +(int)(Math.random() * (40 - 20) + 1); 
		//On affiche une chaine de caract�re qui nous dire que le pok�mon n'a plus d'energie lorsque celui ci est inf�rieur � 0
		if (this.energie < 0) {
			{parler(getNom()+ " n'a plus d'�nergie");
			energie=0;
			}
		}

	}
	
	//Cette methode nous permettra de savoir si le pok�mon est vivant ou non
	public boolean isAlive() {
		if (this.energie > 0) {
			return true;
		}

		else {
			return false;
		}
	}

	//Cette methode nous permettra de savoir combien de cycle de vie un peu pokemon peut vivre
	public void cycleVie( ) {
		int cycle = 0;
		while (energie > 0) {
			vivre();
			manger();
			cycle=cycle+1;
		}
		parler(getNom()+" a v�cu "+cycle+" cycle(s)");

	}
	//Cette methode nous permettra de d�finir la perte d'energie au cours d'un combat
	public void perdreEnergie(int perte) {
		if (perte > energie) {
			energie = 0;

		}else {
			energie = energie - perte;
		}

		if (furie==true) {
			perte=(int) (perte*1.5);
		}

	}
	public int ChoixActionJoueur () {
		//Cet methode permettra au joueur de choisir une action
		System.out.println(getNom()+" : Faut t'il attaquer(1) ou Nourrir(2) le pok�mon ?");
		return action=scan.nextInt();

	}
	//Cette methode permet au pokemon de choisir lequel attaquer
	public int SelectionAdversaire(Pokemon Pokemon1, Pokemon Pokemon2)
	{
		System.out.println("Faut t'il attaquer "+Pokemon1.getNom()+ " (1) ou bien " +Pokemon2.getNom()+" (2)");
		return choisir=scanchoisir.nextInt();	
	}

	//Cette methode permet au pok�mon d'attaquer
	public void attaquer(Pokemon adversaire) {
		adversaire.perdreEnergie(getPuissance());
		puissance = puissance - (0 + (int)(Math.random() * ((1 - 0) + 1)));
		if (puissance <= 0) {
			puissance = 1;
		}

		//On ajoute une condition pour d�clencher la furie du pokemon si l'energie est inf�rieur a 25% et on double la puissance
		if (energie < ((maxEnergie * 25)/100) && furie==false ) {
			furie=true;
			puissance = puissance * 2;
			System.out.println(getNom()+ " se met en furie !");
		}


	}

	//Cette methode nous permettre de combat le pokemon adverse
	public void CombatJoueur (Pokemon adversaire) {
		adversaire.perdreEnergie(getPuissance());
		puissance = puissance - (0 + (int)(Math.random() * ((1 - 0) + 1)));
		if (puissance <= 0) {
			puissance = 1;
		}


	}



}

