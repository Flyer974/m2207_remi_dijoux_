package tp4;

public class CombatMultiPokemon {

	public static void main(String[] args) {
		//On cr�e quatre objets Pokemon. Il y a 2 pokemons qui appartiendront au premier Joueur et les 2 autre au deuxi�me Joueur 
		Pokemon P1J1 = new Pokemon("Vladimir Poutine");
		Pokemon P2J1 = new Pokemon("Donald Trump");
		Pokemon P1J2 = new Pokemon ("Kim Jong-un");
		Pokemon P2J2 = new Pokemon ("Deng Xiaoping");
		
		//On utilise les m�thodes "NomCombattant1(), NomCombattant2(), NomCombattant3() et NomCombattant4()" pour d�finir le nom des pok�mons
		P1J1.NomCombattant1();
		P2J1.NomCombattant2();
		P1J2.NomCombattant3();
		P2J2.NomCombattant4();
		
		int round=0;
		//On ajoute une boucle qui demandera que les pok�mons restent en vie pour continuer � combattre
		while ((P1J1.isAlive() == true || P2J1.isAlive() == true ) && (P1J2.isAlive() == true || P2J2.isAlive() == true))  {
			//On ajoute un compteur
			round++;
			System.out.println("--------------------------------------------------------------------------------------------------");
			System.out.println("Round "+round);
			//On affiche une chaine de caract�re pour afficher l'�tat des pokemons
			System.out.println(("Etat des combatants du Joueur 1 : "+P1J1.getNom() + " (en) " + P1J1.getEnergie() + " (atk) " + P1J1.getPuissance() + " / "+ P2J1.getNom() + " (en) " + P2J1.getEnergie() + " (atk) " + P2J1.getPuissance()));
			System.out.println(("Etat des combatants du Joueur 1 : "+P1J2.getNom() + " (en) " + P1J2.getEnergie() + " (atk) " + P1J2.getPuissance() + " / "+ P2J2.getNom() + " (en) " + P2J2.getEnergie() + " (atk) " + P2J2.getPuissance()));
//Pok�mon 1
			//Cette condition permettra au premier pok�mons du joueur 1 de se battre seulement si son �nergie n'est pas inf�rieur � 0
			if (P1J1.getEnergie()>0) {
				//Ceci permettra de d�finir l'action du joueur 1
			P1J1.ChoixActionJoueur();
			//Si l'action est �gal � 1 alors le dresseur pourra attaquer le premier pok�mon du joueur 2
			if (P1J1.action==1) {
				P1J1.SelectionAdversaire(P1J2, P2J2);
				
				if (P1J1.choisir==1) {
					P1J1.CombatJoueur(P1J2);
					System.out.println(P1J1.getNom()+ " attaque "+ P1J2.getNom());


					//Si l'action �gal � 2 alors le pokemon attaquera le deuxi�me pokemon du joueur 2
				}else if (P1J1.choisir==2) {
					P1J1.CombatJoueur(P2J2);
					System.out.println(P1J1.getNom()+ " attaque "+ P2J2.getNom());

				}
				else P1J1.SelectionAdversaire(P1J2, P2J2);
				//Si le joueur 1 choissit la deuxi�me action alors il pourra nourrir son pok�mon et donc recup�rer de l'�nergie
			}else if (P1J1.action==2){
				P1J1.manger();	
				System.out.println(P1J1.getNom()+ " mange");
			}else {
				P1J1.ChoixActionJoueur();
			}
			}
			
//Pok�mon 2
			if (P2J1.getEnergie()>0) {
			P2J1.ChoixActionJoueur();
			if (P2J1.action==1) {
				P2J1.SelectionAdversaire(P1J2, P2J2);
				if (P2J1.choisir==1) {
					P2J1.CombatJoueur(P1J2);
					System.out.println(P2J1.getNom()+ " attaque "+ P1J2.getNom());



				}else if (P2J1.choisir==2) {
					P2J1.CombatJoueur(P2J2);
					System.out.println(P2J1.getNom()+ " attaque "+ P2J2.getNom());

				}
				else P1J2.SelectionAdversaire(P1J2, P2J2);

			}else if (P2J1.action==2){
				P2J1.manger();	
				System.out.println(P2J1.getNom()+ " mange");
			}else {
				P2J1.ChoixActionJoueur();
			}
			}
			
//Pok�mon 3
			if (P1J2.getEnergie()>0) {
			P1J2.ChoixActionJoueur();
			if (P1J2.action==1) {
				P1J2.SelectionAdversaire(P1J1, P2J1);
				if (P1J2.choisir==1) {
					P1J2.CombatJoueur(P1J1);
					System.out.println(P1J2.getNom()+ " attaque "+ P1J1.getNom());



				}else if (P1J2.choisir==2) {
					P1J2.CombatJoueur(P2J1);
					System.out.println(P1J2.getNom()+ " attaque "+ P2J1.getNom());

				}
				else P1J2.SelectionAdversaire(P1J1, P2J1);

			}else if (P1J2.action==2){
				P1J2.manger();	
				System.out.println(P1J2.getNom()+ " mange");
			}else {
				P1J2.ChoixActionJoueur();
			}
			}
			
//Pok�mon 4
			if (P2J2.getEnergie()>0) {
			P2J2.ChoixActionJoueur();
			if (P2J2.action==1) {
				P2J2.SelectionAdversaire(P1J1, P2J1);
				if (P2J2.choisir==1) {
					P2J2.CombatJoueur(P1J1);
					System.out.println(P2J2.getNom()+ " attaque "+ P1J1.getNom());



				}else if (P2J2.choisir==2) {
					P2J2.CombatJoueur(P2J1);
					System.out.println(P1J2.getNom()+ " attaque "+ P2J1.getNom());

				}
				else P2J2.SelectionAdversaire(P1J1, P2J1);

			}else if (P2J2.action==2){
				P2J2.manger();	
				System.out.println(P1J2.getNom()+ " mange");
			}else {
				P2J2.ChoixActionJoueur();
			}
		}
		}
		
		
		//R�sultat pour savoir le vainqueur	
		System.out.println("-----------------------------");
		if ((P1J1.isAlive() == true || P2J1.isAlive() == true)  && (P1J2.isAlive() == false && P2J2.isAlive() == false)) {
			System.out.println("Le joueur 1 gagne le match");
		}else if ((P1J1.isAlive() == false && P2J1.isAlive() == false ) && (P1J2.isAlive() == true || P2J2.isAlive() == true)) {
			System.out.println("Le joueur 2 gagne le match");
		}else  {
			System.out.println("C'est un match nul");
		}



	}

}
