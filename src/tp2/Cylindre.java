package tp2;

//Extends nous permettra d'obtenir les attributs de class Forme
public class Cylindre extends Cercle {

//Attributs

	//On d�finis l'attribut "hauteur" de type "double" et on l'initialise � une valeur de "1.0"
	double hauteur=1.0;

//Constructeur

	//On cr�e un nouveau constructeur pour le cylindre
	public Cylindre() {
		//Super nous permettra de prendre les attributs de la class "Cercle"
		super();


	}

	//Ce constructeur nous permettra de d�finir la valeur des variables "r", "h", "couleur" et "coloriage"
	public Cylindre(double h, double r , String couleur, boolean coloriage) {
		//Super va nous permettre de prendre les attributs des variables "r", "couleur" et "coloriage" dans la class "Cercle"
		super(r, couleur, coloriage);
		//On d�finit "hauteur" � une variable "h"
		this.hauteur=h;


	}
//Accesseur
	
	//Cet accesseur nous permettra de retourner la valeur de la hauteur
	public double getHauteur() {
		return hauteur;
	}

	//Cet acceseur nous permettra de d�finir la valeur de la hauteur
	public void setCylindre(double h) {
		hauteur=h;
	}

//Methode
	
	//Cette m�thode renvera une chaine de caract�re
	public String seDecrire() {		
		//"super.seDecrire" permettra de reprendre la methode "seDecrire" qui est d�j� d�finit dans la class Cercle
		return "Un Cylindre de hauteur de " + hauteur + " et il a un " + super.seDecrire();

	}

	//Cette m�thode permettra de calculer le volume du cylindre
	public double calculerVolume() {
		double volume;
		volume = hauteur * super.caculerAire();
		return volume;


	}

}
