package tp2;

public class Forme {
	
//On d�clare les diff�rents attributs
	
	//On d�clare un attribut couleur de type String 
	private String couleur;
	//On d�clare un attribut coloriage de type boolean
	private boolean coloriage;
	//On d�clare un attribut total de type entier qui est statique qui permettra d'appeler une fonction sans avoir besoin d'instancier la classe dans une variable
	// On initialise �galement cet entier � 0 pour le compteur
	public static int total = 0;


//Constructeur
	
	//On cr�e un constructeur forme qui aura des param�tres par d�faut
	public Forme () {
		//On initialise "couleur" � "orange"
		this.couleur="orange";
		//On initialise "coloriage" � "true"
		this.coloriage=true;
		//Cet attribut va nous permettre de compter le nombre de type forme cr��s
		total++;

	}
	//On cr�e un deuxi�me constructeur qui contiendra deux variables
	public Forme (String c, boolean r) {
		//On initialise "couleur" � la variable "c"
		couleur=c;
		//On initialise "coloriage" � la variable "r"
		coloriage=r;
		//Cet attribut va nous permettre de compter le nombre de type forme cr��s
		total++;
	}

//Accesseurs

	//Cet accesseur nous permettra d'obtenir de retourner la valeur de la couleur
	public String getCouleur() {
		return couleur;

	}

	//Cet accesseur nous permettra de d�finir la couleur
	public void setCouleur(String c) {
		couleur=c;

	}
	
	//Cet accesseur nous permettra de retourner la valeur de "coloriage"
	public boolean isColoriage() {
		return coloriage;

	}

	//Cet accesseur nous permettra de d�finir la valeur du boolean "coloriage"
	public void setColoriage(boolean b) {
		coloriage=b;

	}
	 //Cette m�thode nous permettra de retourner une cha�ne de caract�re
	public  String seDecrire() {
		return "Une Forme de couleur " + couleur + " et de coloriage " + coloriage;


	}
	//Cette m�thode nous permettra de retourner la valeur total du nombre de type de forme cr�es
	public static int getTotal() {
		return total;
	}



}
