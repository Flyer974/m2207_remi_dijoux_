package tp2;

public class TestForme {

	public static void main(String[] args) {
		//On cr�e 2 objets f1 et f2
		Forme f1 = new Forme();
		//On donne les attributs "vert" et "false" � l'objet f2
		Forme f2 = new Forme("vert", false);
		
	//Exercice 1.4
		System.out.println("Exercice 1.4");
		//On affiche des chaines de caract�re avec les bonnes m�thodes
		System.out.println("f1 : " + f1.getCouleur() + " - " + f1.isColoriage());
		System.out.println("f2 : " + f2.getCouleur() + " - " + f2.isColoriage());
		
	//Exercice 1.5
		System.out.println("\nExercice 1.5");
		//On d�finis gr�ce � la m�thode "setCouleur" la couleur "rouge"
		f1.setCouleur("rouge");
		//On d�finis gr�ce � la m�thode "setColoriage" le coloriage "false"
		f1.setColoriage(false);
		System.out.println("Voici le nouveau f1 : " + f1.getCouleur() + " - " + f1.isColoriage());

	//Exercice 1.7
		System.out.println("\nExercice 1.7");
		//On affiche des chaines chaines de caract�re avec les bonnes m�thodes qui nous affichera ensuite les d�tails des formes f1 et f2
		System.out.println("f1 : " + f1.seDecrire());
		System.out.println("f2 : " + f2.seDecrire());

	//Exercice 2.6
		System.out.println("\nExercice 2.6");
		//On cr�e un nouvel objet "c1"
		Cercle c1 = new Cercle();
		//On affiche la chaine de caract�re avec la bonne m�thode
		//Ceci nous affichera les d�tails du cercle "c1"
		System.out.println("c1 : " + c1.seDecrire());

	//Exercice 2.8
		System.out.println("\nExercice 2.8");
		//On cr�e un nouvel objet "c2"
		Cercle c2 = new Cercle();
		
		//On change la valeur du rayon avec la m�thode "setCercle"
		c2.setCercle(2.5);
		//On affiche la chaine de caract�re avec la bonne m�thode qui nous affichera ensuite les d�tails du cercle "c2"
		System.out.println("c2 : " + c2.seDecrire());

	//Exercice 2.10
		System.out.println("\nExercice 2.10");
		//On cr�e un objet "c3"
		Cercle c3 = new Cercle(3.2 , "jaune" , false);
		//On affiche des chaines de caract�re avec les bonnes m�thodes
		//Ceci affichera les d�tails du cercle "c3"
		System.out.println("c3 : " + c3.seDecrire());
		//Ceci affichera le perim�tre du cercle 3
		System.out.println("Voici le p�rim�tre du cercle c3 : " + c3.calculerPerimetre());
		//Ceci affichera l'aire du cercle 3
		System.out.println("Voici l'aire du du cercle c3 :  " + c3.caculerAire());

	//Exercice 3.2
		System.out.println("\nExercice 3.2");
		//On cr�e un objet "cy1"
		Cylindre cy1 = new Cylindre();
		//On affiche la chaine de caract�re avec la bonne m�thode qui nous montrera ensuite les d�tails du cylindre cy1
		System.out.println(cy1.seDecrire());
		
	//Exercice 3.4
		System.out.println("\nExercice 3.4");
		//On cr�e un objet "cy2"
		Cylindre cy2 = new Cylindre(1.3, 4.2, "bleu", true);
		//On affiche la chaine de caract�re avec la bonne m�thode qui nous montrera ensuite les d�tails du cylindre cy2
		System.out.println(cy2.seDecrire());

	//Exercice 3.5
		System.out.println("\nExercice 3.5");
		//On affiche des chaines chaines de caract�re avec les bonnes m�thodes qui nous montrera ensuite les diff�rents volumes
		System.out.println("Le cylindre cy1 � pour volume " + cy1.calculerVolume());
		System.out.println("Le cylindre cy2 � pour volume " + cy2.calculerVolume());

	//Exercice 4
		System.out.println("\nExercice 4");
		//On affiche la chaine de caract�re avec la bonne m�thode qui nous montrera ensuite le nombre d'objet de type formes cr�es
		System.out.println("Voici le nombre de Forme cr�es : " + Forme.getTotal());
		

	}

}
