package tp2;

//Extends nous permettra d'obtenir les attributs de class Forme
public class Cercle extends Forme {

//Attributs
	
	//D�claration du rayon de type double
	private double rayon;


//Constructeur
	
	//Constructeur qui d�finira les valeurs par d�fauts pour notre cercle
	public Cercle() {
		//Super va nous permettre d'obtenir tous les attributs d�finis dans class Forme
		super();
		//On initialise la valeur du rayon � 1
		this.rayon=1;

	}
	//Ce contructeur nous permettra de d�finir la valeur de la variable "r" 
	public Cercle(double r) {
		//Super va nous permettre d'obtenir tous les attributs d�finis dans class Forme
		super();
		//On d�finit l'attribut "rayon" dans la variable "r"
		rayon=r;
	}

	//Ce constructeur nous permettra de d�finir la valeur des variables "r", "couleur" et "coloriage"
	public Cercle(double r, String couleur, boolean coloriage) {
		//Super va nous permettre de prendre les attributs de couleur et coloriage dans la class Forme
		super(couleur, coloriage);
		rayon=r;

	}

//Acceseurs

	//Acceseur qui nous renvera la valeur du rayon 
	public double getCercle() {
		return rayon;
	}

	//Accesseur qui nous permettra de d�finir la valeur du rayon
	public void setCercle(double r) {
		rayon=r;
	}

//Methodes

	//M�thode qui nous permettra d'obtenir une chaine de caract�re
	public String seDecrire() {
		//On reprend les m�thodes "getCouleur()" et "isColoriage()" de la class Forme
		return "Un Cercle de rayon " + rayon + " qui est issue d�une Forme de couleur " + getCouleur() + " et de coloriage " + isColoriage();
	}
	
	//M�thode qui affichera une chaine de caract�re
	public void Affiche() {
		System.out.println("Le cercle � un rayon de " + rayon);
	}

	//M�thode qui nous permettra d'obtenir la valeur du perimetre du cercle
	public  double calculerPerimetre() {
		double perimetre;
		return perimetre = 2*Math.PI*rayon;	

	}

	//M�thode qui nous permettra d'obtenir l'aire du cercle 
	public double caculerAire( ) {
		double aire;
		return aire = Math.PI*(rayon*rayon);
	}


}
