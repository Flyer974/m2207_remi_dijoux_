package tp3;

//Extends va nous permmetre d'utiliser les attibuts de la class Humain
public class Dame extends Humain{

//Attributs
	
	//On d�clare l'attribut liberte de type boolean
	protected boolean liberte;

	
//Accesseur
	
	//Cet accesseur va nous permettre de d�finir la valeur de l'attribut "nom"
		public String quelEstTonNom() {
			return "Miss " +nom;
		}
		
//Constructeur
	
	//Ce constructeur va nous permettre de d�finir le nom d'une Dame
	public Dame(String nom) {
		//super va nous permettre de prendre les attributs de la class humain
		super(nom);
		//On d�finit la valeur de l'attribut "boisson"
		this.boisson="Martini";
		//On initialise l'attribut liberte � "true" pour montrer que la dame est en libert�
		liberte=true;
	}
	
	//Cette methode va nous permettre de d�finir la prise en otage
	public void priseEnOtage() {
		//On initialise la valeur de liberte � "false" pour montrer que la dame subit une prise d'otage
		liberte=false;
		//On utilise l'accesseur parler pour �crire une chaine de caract�re. Ceci va nous permettra d'afficher une chaine de caract�re pour le dialogue de la dame
		parler((nom+" - Au secours !"));
	}
	
	//Cette methode va nous permettre de d�finir la lib�ration de la dame
	public void estLiberee() {
		//On initialise la valeur de liberte � "true" pour montrer que la dame est en libert�
		liberte=true;
		//On utilise l'accesseur parler pour �crire une chaine de caract�re. Ceci va nous permettra d'afficher une chaine de caract�re pour le dialogue de la dame
		parler((nom+ " - Merci Cowboy"));
		}
	
	//Cette methode va nous permettre de presenter le personnage
	public void sePresenter ( ) {
		//super va nous permettre de reprendre la methode "sePresenter" de la class Humain
		super.sePresenter();
		//On d�finit des conditions pour d�finir la lib�ration ou le kidnapping de la dame
		if (liberte == true) {
			//Si la valeur de l'attribut de liberte est initialis� � true alors la dame va utiliser la methode parler pour dire quelle est libre
			parler(nom+ " - Actuellement je suis libre");
			//Si la valeur de l'attribut de liberte n'est pas initialis� � true alors la dame va utiliser la methode parler pour dire quelle est kidnapp�e
		}else {parler(nom+" - Actuellement je suis kidnapp�e");
		}
		
	}

}
