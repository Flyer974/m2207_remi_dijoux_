package tp3;

public class Cowboy extends Humain{

//Attributs
	
	//On d�finit l'attribut popularite de type int
	protected int popularite;
	//On d�finit l'attribut caract de type String
	public String caract;
	
//Constructeur
	
	//On d�finit le constructeur Cowboy avec un param�tre "n" de type nom
	public Cowboy(String nom) {
		//super va nous permettre de prendre les attributs de la class humain
		super(nom);
		//On d�finit la valeur de l'attribut "boisson"
		this.boisson="whiskey";
		//On initialise la popularit� � 0
		popularite=0;
		//On initialise l'attribut caract (qui correspond au caract�re) � "vaillant"
		caract="vaillant";
		
	}
	
//Methode
	
	//Cette methode permettra au sherif de tirer sur le brigand. On aura l'objet brigand comme param�tre
	public void tire(Brigand brigand) {
		//On utilise l'accesseur parler pour d�finir une chaine de caract�re
		parler(("Le " +caract+ " " +nom+ " tire sur " +brigand.quelEstTonNom()+ " PAN !" ));
		parler(nom+" - Prend �a voyou !");
	}
	
	//Cette methode permettra au sh�rif de liber� la dame
	public void libere(Dame dame) {
		//On utilise la methode "est Libereree" de la class dame pour montrer qu'elle est liber�
		dame.estLiberee();
		//On augmente la popularit� de 10 � chaque fois que le sh�rif sauve une dame
		popularite+=10;
	}
	
}
