package tp3;

public class Sherif extends Cowboy {
//Attributs
	
	//On d�finit un attribut arrestation de type int
	int arrestation;
	

	//On d�finit un constructeur qui prendra en param�tre le un nom
	public Sherif(String nom) {
		super(nom);
		this.boisson="whiskey";
		//On initialsie le nombre d'arrestation du sh�rif � 0
		arrestation=0;
		//On initialsie la popularit� du sh�rif � 0
		popularite=0;
		
	}
//Accesseur
	
	//cet accesseur nous permettra d'obtenir le nom du sh�rif
	public String quelEstTonNom() {
		return "Sherif " +nom;
	}
//Methodes
	
	//Cette m�thode nous permettra de pr�senter le sh�rif
	public void sePresenter() {
		super.sePresenter();
		parler(nom+(" - Je suis vaillant et ma popularit� est " +popularite));
		parler(nom+" - J'ai d�ja arr�t� " +arrestation+ " brigand(s)");
	}
	
	//Cette m�thode nous permettra d'arr�ter un briband
	public void coffrer(Brigand b) {
		//Le nombre d'arrestion du sh�rif augmente de 1 � chaque arrestation d'un brigand
		arrestation+=1;
		// "b.nom" nous permettra d'obtenir le nom du brigant qui a �t� arr�t�
		parler(nom+" - Au nom de la loi, je vous arr�te "+ b.nom);
	}
	
	
}
