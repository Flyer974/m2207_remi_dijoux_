package tp3;

public class Humain {
	
//Attributs
	
	//On d�clare l'attribut "boisson" de type String
	protected String boisson;
	//On d�clare l'attribut "nom" de type String
	protected String nom;
	
//Constructeur
	
	//On cr�e un nouvel constructeur Humain avec un param�tre "nom"
	public Humain(String nom) {
		//On d�finit le "nom" qu'on a d�clar� dans l'autre variable "nom"
		this.nom=nom;
		//On d�finit la valeur de l'attribut boisson � une chaine de caract�re "lait"
		this.boisson="lait";
	}
	
//Accesseurs
	
	//Cet accesseur va nous permettre de d�finir la valeur de l'attribut "nom"
	public String quelEstTonNom() {
		//On retourne la valeur de l'attribut nom
		return nom;
	}
	
	//Cet accesseur va nous permettre de d�finir la valeur de l'attribut "boisson"
	public String quelleEstTaBoisson() {
		//On retourne la valeur de l'attribut boisson
		return boisson;
	}
	
	
	
	//Cette accesseur va nous eviter dans notre programme d'utiliser System.out.println()
	public void parler(String texte) {
		System.out.println(texte);
	}
	
//Methodes

	//Cette methode va nous permettre de presenter les diff�rents personnages humains
	public void sePresenter() {
		//On a une chaine de caract�re qui utilise les accesseur "quelEstTonNom" et "quelleEstTaBoisson" pour la pr�sentation d'un personnage
		parler(nom+" - Bonjour je suis " +quelEstTonNom()+(" et ma boisson pr�fer�e est le "+quelleEstTaBoisson()));
	}
	
	//Cette m�thode va nous permettre d'afficher une chaine une caract�re
	public void boire() {
		//On utilise l'accesseur parler pour ecrire une chaine de caract�re. On utilise les accesseurs "quelEstTonNom()" et "quelleEstTaBoisson()" �galement pour compl�ter la chaine de caract�re
		parler(quelEstTonNom()+ " - Ah ! un bon verre  de " + quelleEstTaBoisson()+ " GLOUPS !");
	}

}
