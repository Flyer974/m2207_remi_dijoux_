package tp3;

public class Prison {
//Attributs 
	
	//Cet attribut aura un nom "prison" de type Brigand pour mettre les brigand en prison
	private Brigand[] prison;
	//On d�clare un attribut nom de type String
	private String nom;
	//On d�clare un attribut nombrePrisonnier de type int
	private int nombrePrisonnier;
	
//Constructeur
	
	//On a un constructeur qui prendra comme param�tre un nom "n"
	public Prison(String n) {
		this.nom=n;
	}
	
//Accesseurs
	
	//Cet accesseur nous permmettra d'obtenir le nom
	public String obtenirNom () {
		return nom;
	}
	
	//Cet accesseur nous permmettra d'obtenir le nombre de prisonnier. Il utilisera les methodes "nomreDePrisonnier()" et "obtenirNom())
	public void savoirNombrePrisonniers() {
		System.out.println("Il y a "+ nombredePrisonniers()+" brigand(s) dans la prison "+obtenirNom());
	}
	//Cet accessuer nous retournera la valeur du nombre de prisonniers
	public int nombredePrisonniers() {
		return nombrePrisonnier;
	}
	
//Methodes
	
	//Cette m�thode nous permettra de mettre le brigand en cellule
	public void mettreEnCellule(Brigand b) {
		//On d�finit le tableau qui contiendra les prisonniers
		prison [nombrePrisonnier]=b;
		//On aura une chaine de caract�re qui nous donnera le nom du prisonnier et sa cellule
		System.out.println(b.quelEstTonNom()+ " est mis dans la cellule" +nombredePrisonniers());
		//Ce compte nous permettra d'ajouter une prisonnier lorsqu'on utilisera la methode mettreEnCelulle
		nombrePrisonnier++;
		//On utilise la methode savoirNombrePrisonniers
		savoirNombrePrisonniers();
	}
	
	//Cette m�thode nous permettra de sortir le brigand de sa cellule et il prendra en param�tre b pour d�finir une brigand
	public void sortirDeCellule(Brigand b) {
		//On d�finit le tableau qui contiendra les prisonniers
		prison [nombrePrisonnier]=b;
		//On affiche si le prisonnier est sortit de sa cellule
		System.out.println(b.quelEstTonNom() + "est sorti de sa cellule");
		//On utilise la methode savoirNombrePrisonniers
		savoirNombrePrisonniers();

	}
	
	
	
	
	


}
