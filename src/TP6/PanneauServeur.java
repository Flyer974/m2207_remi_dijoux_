package TP6;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.ServerSocket;



public class PanneauServeur extends JFrame implements ActionListener{

	//Attributs

	JButton b0 = new JButton("Exit");
	Container panel = getContentPane();
	JTextArea text = new JTextArea("Le panneau est actif"); 

	public PanneauServeur() {
		super();
		//On met un titre � notre application
		this.setTitle("Serveur - Panneau d'affichage");
		//On choisit la dimension de la fen�tre
		this.setSize(400,300);
		//On choisit l'emplacement de la fen�tre � partir du coin en haut � gauche
		this.setLocation(100,200);
		//Cela va permettre de terminer l'application � la fermeture de la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//On rend la fen�tre visible
		JScrollPane scroll = new JScrollPane(text);
		panel.add(scroll, BorderLayout.CENTER);
		b0.addActionListener(this);
		panel.add(b0,  BorderLayout.SOUTH);


		try {
			ServerSocket serveur = new ServerSocket(8888);
			text.append("\nServeur d�marr�");
		} catch (IOException e) {

			e.printStackTrace();
			System.out.println("Erreur de cr�ation ServerSocket");
		}

		this.setVisible(true);

	}


	public void actionPerformed(ActionEvent e) {
		System.out.println("Une action a �t� d�tect�e");
		System.exit(-1);


	}




	public static void main(String[] args) {
		PanneauServeur serveur = new PanneauServeur();


	}

}
