package TP6;

public class TestExceptions {

	public static void main(String[] args) {
		int x = 2, y = 0;
		try {
		System.out.println("y/x = " + y/x);
		System.out.println("x/y = " + x/y);
		System.out.println("Commande de fermeture du programme");
		}
		catch (Exception e) {
		System.out.println("Une exception a �t� captur�e");
		}
		System.out.println("Fin du programme");
	}

}
