package tp1;

public class ClientMultiComptes {

	//Les attributs

	//On d�clare "nom" de type String pour signifier que c'est une chaine de caract�re
	public String nom;
	//On d�clare "prenom" de type String pour signifier que c'est une chaine de caract�re
	public String prenom;
	//On cr�e un objet "compte" qui contiendra au maximun 10 comptes
	private Compte[] tabcomptes = new Compte[10];
	//On d�clare un entier "nbcompte" pour le num�ro du compte bancaire
	private int nbcompte;
	//On initialise l'objet "Compte" � "compteCourant"
	private Compte compteCourant;
	// On initialise et d�clare un double � "solde"
	private double solde;





	// On cr�e un Constructeur pour afficher les informations des Clients avec plusieurs comptes bancaire

	public ClientMultiComptes (String n, String p, Compte compte) {
		//On initialise et d�clare un tableau "tabcomptes" qui contiendra le premier compte bancaire
		tabcomptes [0] = compte;
		//Ceci va nous permettre d'ajouter les comptes suivants
		nbcompte ++;

		//On d�clare "this.nom" dans la variable n
		this.nom=n;
		//On d�clare "this.prenom" dans la variable p
		this.prenom=p;
		//On d�clare "this.compteCourant" dans la variable compte
		this.compteCourant=compte;				
	}
	//On a une m�thode ajouter un compte bancaire
	public void ajouterCompte(Compte c) {
		tabcomptes [nbcompte] = c;

	}

	//Cette methode nous permettra d'obtenir la valeur de la solde
	public  double getSolde() {
		//On retourne la valeur de la solde
		return solde;
	}

}