package tp1;

public class MaBanque {

	public static void main(String[] args) {

		//Cr�ation d'un compte
		Compte c;
		c = new Compte (1);

		//Affichage du d�couvert
		System.out.println("Voici votre d�couvert : " + c.getDecouvert());

		//Changement de valeur du d�couvert et affichage de la solde
		c.setDecouvert(100);
		c.afficherSolde();

		System.out.println("Voici votre d�couvert : " + c.getDecouvert());

		//On fait un retrait de 5, on ajoute de l'argent au d�pot et on affiche la solde
		System.out.println(c.retrait(5));
		c.depot(150);
		c.afficherSolde();

		//Cr�ation du Compte num�ro 2
		Compte c2;
		c2 = new Compte (2);

		//Message pour espacer les diff�rents r�sultats
		System.out.println(" ");

		//On depose de l'argent au d�pot
		c2.depot(1000);
		//On affiche la solde
		c2.afficherSolde();
		//On fait un retrait de 600 et 700
		System.out.println(c2.retrait(600));
		System.out.println(c2.retrait(700));
		//On change la somme du d�couvert
		c2.setDecouvert(500);
		//On fait un retrait de 700
		System.out.println(c2.retrait(700));

		//Message pour espacer les diff�rents r�sultats
		System.out.println(" ");

		//On cr�e un nouveau client
		Client client1 = new Client("Julie", "payet", c2);
		//On affiche les infos du client
		System.out.println("Voici le compte de Julie Payet : ");
		//On affichage la solde du client
		client1.afficherSolde();





	}

}
