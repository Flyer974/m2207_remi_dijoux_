package tp1;

public class TestClient {

	public static void main(String[] args) {

		//On cr�e un nouvel objet "compteclient"
		Compte compteclient = new Compte (1);


		// On cr�e un deuxi�me objet "c" qui corespont au compte client
		Client c;
		c = new Client ("Leon", "Paul", compteclient);

		//On affiche un message pour obtenir le nom, le prenom et la solde du client
		System.out.println(c.getprenom() + " " + c.getnom()+ " � une solde de " + c.getSolde());


		//Creation d'un troisi�me objet "multicompte" qui aura diff�rents attributs sp�cifique au client
		ClientMultiComptes multicompte = new ClientMultiComptes("Alfredo ", "Shishi", compteclient);
		//On affiche le nom et le prenom du propri�taire du multi compte bancaire
		System.out.println(multicompte.nom + multicompte.prenom);



	}

}
