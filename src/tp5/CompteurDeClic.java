package tp5;

import javax.swing.JFrame;


import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;



public class CompteurDeClic extends JFrame implements ActionListener{
	//Attributs
	int nombreClic = 0;
	
	Container panel = getContentPane();
	
	JButton b0 = new JButton("Click !");
	JLabel monLabel = new JLabel("Vous avez cliqu� " + this.nombreClic+ " fois"); 

	public CompteurDeClic() {
		super ();
		setSize(300,100);
		setTitle("Compteur de clique");
		setLocation(100,100);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);

		panel.setLayout(new FlowLayout());
		panel.add(b0);
		panel.add(monLabel);
		b0.addActionListener(this);




	}

	//Methodes
	public void actionPerformed(ActionEvent e) {
		System.out.println("Une action a �t� d�tect�e");
		this.nombreClic++;
		monLabel.setText("Vous avez cliqu� " + this.nombreClic + " fois"); // Modification du label



	}
}
