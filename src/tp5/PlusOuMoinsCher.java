package tp5;

//Jframe va nous permettre de g�rer les f�netres graphique
import javax.swing.JFrame;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.util.Scanner;

public class PlusOuMoinsCher extends JFrame implements ActionListener {
	//Attributs
	int nbmax;
	int nbdeviner;

	//On cr�e un nouveau panel
	Container panel = getContentPane();

	//On cr�e deux nouveau labels
	JLabel monLabel = new JLabel(" Votre proposition : "); 
	JLabel monLabel2 = new JLabel(" La r�ponse"); 
	//On cr�e une zone de texte
	JTextField monTextField = new JTextField();
	//On cr�e un nouveau button qui v�rifiera le r�sultat
	JButton b0 = new JButton(" V�rifier !");


//Constructeur
	public PlusOuMoinsCher() {
		//On va h�riter des caract�re de JFrame
		super ();
		//On d�finit la taille de la fen�tre
		this.setSize(300,150);
		//On ajoute un titre � la fen�tre
		this.setTitle("Plus cher ou moins cher");
		//On choisit l'emplacement de la fen�tre
		this.setLocation(100,100);
		//Cela va permettre de terminer l'application � la fermeture de la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Cela va nous permettre de rendre le fen�tre visible
		this.setVisible(true);
		//On ajoute la methode init() pour ajouter un nombre compris en 1 et 100
		this.init();
		// On ajoute 2 lignes et 2 colones
		panel.setLayout(new GridLayout(2, 2));
		//On ajoute dans le panneau un champ de texte
		panel.add(monTextField);
		//On ajoute les 2 labels pour afficher du texte
		panel.add(monLabel);
		panel.add(monLabel2);
		//On ajoute le bouton b0 au panneau
		panel.add(b0);


		//On d�finis l'emplacement des label, text, bouton dans le panneau
		panel.add(monLabel, BorderLayout.NORTH);
		panel.add(monTextField, BorderLayout.NORTH);
		panel.add(b0, BorderLayout.SOUTH);
		panel.add(monLabel2, BorderLayout.SOUTH);
		//Ceci va nos permettre d'attribuer une action au bouton 0
		b0.addActionListener(this);



	}
//Accesseur
	public void init() {
		nbdeviner = (int)(1 - Math.random() * (1 - 100) + 1);;
		nbmax = 8;
	}
	
//Methodes
	
	//On cr�e une methode pour trouver le bon chiffre
	public void actionPerformed(ActionEvent e) {
		//On affiche une chaine de caract�re pour montrer qu'il y a une action
		System.out.println("Une action a �t� d�tect�e");

		//Cette ligne nous permettra de convertir une chaine de caract�re contenant un entier en int
		int conversion = Integer.parseInt(monTextField.getText());

		//Si le nombre d'essaie est � 0, alors c'est un echec
		if (nbmax == 0) {
			init();
			System.out.println("Echec");
			monLabel2.setText("Echec");

		}
		//Si le chiffre est superieur au nombre qu'il faut deviner, alors on affichera une chaine de caract�re qui nous demandera de choisir un nombre plus bas
		else if (conversion > nbdeviner) {
			nbmax--;
			monLabel2.setText("Moins cher !");
		}
		//Si le chiffre est inf�rieur au nombre qu'il faut deviner, alors on affichera une chaine de caract�re qui nous demandera de choisir un nombre plus haut
		else if (conversion < nbdeviner) {
			nbmax--;
			monLabel2.setText("Plus cher !");
		}

		//Si le chiffre est bon alors on affichera une chaine de caract�re qui nous diras que c'est bon
		else {
			init();
			monLabel2.setText("Vous avez devin� !");

		}
	}

	
	//On cr�e un main pour executer le programme
	public static void main (String[] args) {
		PlusOuMoinsCher app = new PlusOuMoinsCher();
	}


}
